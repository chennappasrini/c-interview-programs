﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sample
{
    
    class A
    {
        public int Add()
        {
            Console.Write(10);
            return 10;
        }
    }

    class B : A
    {
        public int Add()
        {
            Console.WriteLine(11);
            return 11;
        }
    }

     class main {
         public  void Main(string [] args){

             B objb = new B();
             A obja = new A();

             objb.Add();            
             obja.Add();

             Console.Read();

         }
     }
}
